export default class Piece {
    constructor(type, color, blocks) {
        this.type = type;
        this.color = color;
        this.blocks = blocks;
        this.x = 0;
        this.y = 0;
    }

}

[0,1,0],
[0,1,1],
[0,1,0]