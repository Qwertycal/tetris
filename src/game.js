import Grid from './grid.js';
import Peice from './piece.js';

export default class Game {
    constructor(){
        this.score = 0;
        this.level = 0;
        this.lines = 0;
        this.gameOver = false;
        this.grid = new Grid(20,10);
        this.activatePiece = new Peice(
            'T',
            'purple',
            [
                [0,0,0],
                [1,1,1],
                [0,1,0]
            ]);
    }

    movePieceLeft() {
        this.activatePiece.x -=1;
        this.grid.lockPiece(this.activatePiece);
    }

    movePieceRight() {
        this.activatePiece.x +=1;
        this.grid.lockPiece(this.activatePiece);
    }

    movePieceDown() {
        this.activatePiece.y +=1;
        this.grid.lockPiece(this.activatePiece);
    }
}

